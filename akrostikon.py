import sys, time
import cProfile

def get_words(length):
  with open('words.txt', 'r', encoding='ISO-8859-1') as f:
    words = f.readlines()
  words = [w.lower().replace('\n','').replace('\r','') for w in words]
  words = [w for w in words if w.isalpha() and len(w) == length]
  words.sort()
  return words

def get_sequences(seq, start_to_words, find_palindroms):
  if len(seq) == len(seq[0]):
    if find_palindroms:
      is_palindrom = all(seq[i] == seq[-i-1][::-1] for i in range(len(seq)))
      print(('palindrom: ' if is_palindrom else 'not palindrom: ') + str(seq))
      return [seq] if is_palindrom else []
    print(str(seq))
    return [seq]

  n = len(seq)
  for i in range(len(seq[0]) - 1, n - 1, -1):
    start = ''.join(s[i] for s in seq)
    if start not in start_to_words:
      return []

  sequences = []
  for w in start_to_words[start]:
    sequences += get_sequences(seq + [w], start_to_words, find_palindroms)
  return sequences

def akrostikon(length = 6, find_palindroms = False):
  words = get_words(length)
  akrostikon = []

  if find_palindroms:
    words = [w for w in words if w[::-1] in words]

  start_to_words = {}
  for w in words:
    for i in range(1, len(w)):
      start = w[:i]
      start_to_words.setdefault(start, []).append(w)

  print('Num of words: %s' % len(words))

  i = 1
  st = time.time()
  sequences = []
  for word in words:
    if i % 100 == 0:
      print(str(i) + ". " + "".join(word))
      print(str(time.time()-st))
    i += 1
    sequences += get_sequences([word], start_to_words, find_palindroms)

  if sequences:
    with open('akrostikon' + str(length) + ('-palindrom' if find_palindroms else '') + '.txt', 'w') as f:
      for sequence in sequences:
        f.write('\n'.join(sequence) + '\n\n')
  return sequences

if __name__ == '__main__':
  args = sys.argv[1:]
  num_letters = 6
  find_palindroms = False
  for i, arg in enumerate(args):
    if arg == '-n' and i + 1 < len(args):
      num_letters = int(args[i + 1])
    elif arg == '-p' and i + 1 < len(args):
      find_palindroms = bool(args[i + 1])

  akrostikon(num_letters, find_palindroms)
#  cProfile.run('akrostikon(num_letters, find_palindroms)')

